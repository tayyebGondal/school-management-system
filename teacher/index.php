<?php 
    session_start();
    if(!isset($_SESSION['teacher']) || $_SESSION['teacher']==''){
        header('location:../index.php?msg=unauthorized');
        die();
        return false;
    }
include('../connection.php');
 $teacher= $_SESSION['teacher'];
 // var_dump($teacher);
$sql=mysqli_query($conn,"select * from teacher where email='$teacher' ");
$teacherinfo=mysqli_num_rows($sql);
// 

if($teacherinfo > 0){
     
        while($product=mysqli_fetch_array($sql))
            {
            $name = $product['name'];
            $email = $product['email'];
            }
        
}

// 
 
 // var_dump($name);
 // var_dump($email);
?>
 
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Online Notice Board Teacher Dashboard</title>

<!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet"> 

<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
        integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
     
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

<!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script> 

<!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
    </script> 

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
<div class="container-fluid">
<div class="navbar-header">
 
<!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
    aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button> -->
                 
    <a class="navbar-brand" href="#" >Hello <?php echo $name;?></a>
</div>  
    <a style="width: 80%;text-align: center;" class="navbar-brand" href="index.php"><center><B>NOTICE BOARD</B></center></a> 
<div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
    <li><a href="logout.php">Logout</a></li>
    </ul>
                 
<!--<form class="navbar-form navbar-right">
    <input type="text" class="form-control" placeholder="Search...">
</form>-->
             
</div>
</div>
    </nav>
             
<!-- <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
    <li><a href="logout.php">Logout</a></li>
    </ul>
    <form class="navbar-form navbar-right">
    <input type="text" class="form-control" placeholder="Search...">
    </form>
    </div> --> 

</div>
    </nav>
<div class="container-fluid">
<div class="row">
<div class="col-sm-3 col-md-2 sidebar" style="border:2px solid black; border-top:0px">
    <center><ul class="nav nav-sidebar" style="margin-top:50px; color:black;">

<!-- check teacher profile image -->
<?php 
$q=mysqli_query($conn,"select image from teacher where email='$teacher'");
$row=mysqli_fetch_assoc($q);
if($row['image']=="")
{
?>
    <li><a href="index.php?page=update_profile_pic"><img title="Update Your profile pic Click here"
    style="border-radius:50px" src="../images/teacher.jpg" class="img img-responsive img-thumbnail"  width="100" height="100"
    alt="not found" /></a></li>
    <?php 
}
else
{
?>
    <li><a href="index.php?page=update_profile_pic"><img title="Update Your profile pic Click here"
    style="border-radius:50px"
    src="../images/<?php echo $row['image'];?>"
    class="img img-responsive img-thumbnail"   width="100" height="100"  alt="not found" /></a></li>
<?php 
}
?>
 
<b><?php echo $email;?> </b>
<br><br>
    <style type="text/css">.text-style-navbar{color:black;border:2px solid black;font-weight:bolder;                                }
    </style> 

<!-- <li class="active"><a href="index.php" class="text-style-navbar" style="color:black; ">Dashboard <span class="sr-only">(current)</span></a></li> -->
 
<br>
                        
<!-- find teacher' image if image not found then show dummy image -->

<li class="active"><a href="index.php?page=notification" class="text-style-navbar" style="color:white"></span> Notifications</a></li><br>
<li class="active"><a href="index.php?page=update_profile" class="text-style-navbar" style="color:white"> Update Profile</a></li><br>
<li class="active"><a href="index.php?page=update_password" class="text-style-navbar" style="color:white"> Update Password</a></li><br>
</ul>
</center>
</div>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

<!-- container--> 
<?php 
	@$page=  $_GET['page'];
if($page!="")
{
if($page=="update_password")
{
include('update_password.php');
}
if($page=="notification")
{
include('notification.php');
}
if($page=="detail_notification")
{
include('detail_notification.php');
}
if($page=="update_profile")
{
include('update_profile.php');
}
if($page=="update_profile_pic")
{
include('update_profile_pic.php');
}
}   
else
{
?>
<!-- container end-->


<?php 
include('notification.php');
} 
?>

<!-- 
<div class="row">
<div class="col-md-12">
<center>
<div class="col-md-2"   style="border-right:5px solid blue;">
    <b><h4>August</h4> 
    <h6>2021</h6>
</b>
</div>
</center>
<div class="col-md-8">
     <b><h4 style="color:red">Hello You have a new notice please click on it</h4>
     <h5>September, 25, 2021</h5>
</b>
</div>
</div> 
</div> -->

</div>
</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="../js/vendor/jquery.min.js"><\/script>')
    </script>
    <script src="../js/bootstrap.min.js"></script>
     
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../js/vendor/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>