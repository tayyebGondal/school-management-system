<?php  
if(isset($_GET['addNotice']) && $_GET['addNotice'] == 'added'){  
    ?>
<div class="alert alert-success" role="alert">
    <strong>Success!</strong> A New notice has been added successfully. 
</div>
<?php  
}
else if(isset($_GET['addNotice']) && $_GET['addNotice'] == 'not_added'){ ?>
<div class="alert alert-danger" role="alert">
 <strong>Error!</strong> Notice has not been added. 
</div>  
<?php }
if(isset($_GET['UpdateNotice']) && $_GET['UpdateNotice'] == 'updated'){  ?>  
<div class="alert alert-success" role="alert">
    <strong>Success!</strong> An old notice has been updated successfully. 
</div>
<?php  
}
else if(isset($_GET['UpdateNotice']) && $_GET['UpdateNotice'] == 'not_updated'){ ?>
<div class="alert alert-danger" role="alert">
 <strong>Error!</strong> Notice has not been updated. 
</div>   
<?php }
if(isset($_GET['DeleteNotice']) && $_GET['DeleteNotice'] == 'deleted'){  ?> 
<div class="alert alert-success" role="alert">
    <strong>Success!</strong> A notice has been delete successfully. 
</div>
<?php  
}
else if(isset($_GET['DeleteNotice']) && $_GET['DeleteNotice'] == 'not_deleted'){ ?>
<div class="alert alert-danger" role="alert">
 <strong>Error!</strong> Notice has not been delete. 
</div> 
<?php  
}  
?> 


<h1 class="page-header">MANAGE NOTICE</h1>
<div class="row placeholders text-justify"> 

	<a href="index.php?page=add new notice">
<div class="col-xs-6 col-sm-3 placeholder">
	<img src="../images/add new notice.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
<h3>Add New Notice</h3>
<!-- <span class="text-muted">Something else</span> -->
</div>
</a>
	 
    <a href="index.php?page=manage notifications">
<div class="col-xs-6 col-sm-3 placeholder">
    <img src="../images/manage notice.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
<h3>Manage Notifications</h3>			 
<!-- <span class="text-muted">Something else</span> -->
</div>
</a>
</div>

<?php
@$page =  $_GET['page'];
if ($page != "") {
if ($page == "add new notice")  
{
include('add_new_notice.php');
}
if ($page == "manage notifications")  
{ 
include('manage_notifications.php');
}
}
?> 
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<!-- Datatables -->
<!-- <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script> -->
<!-- Bootstrap --> 
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->