<?php
session_start();
if(!isset($_SESSION['admin']) || $_SESSION['admin']==''){
    header('location:../index.php?msg=unauthorized');
    die();
    return false;
}
include('../connection.php');
$admin = $_SESSION['admin']; 

// var_dump($admin);
$sql = mysqli_query($conn, "select * from admin where email='$admin' ");
$admininfo = mysqli_num_rows($sql);
// 
if ($admininfo > 0) {

while ($product = mysqli_fetch_array($sql)) {
    $name = $product['name'];
    $email = $product['email'];
}
}

// 
// var_dump($name);
// var_dump($email);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Online Notice Board Admin Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="../DataTables/datatables.css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
<div class="container-fluid">
<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button>
<a class="navbar-brand" href="#"> Admin Dashboard</a>
</div>

<div id="navbar" class="navbar-collapse collapse">
<ul class="nav navbar-nav navbar-right">
    <li><a href="logout.php">Logout</a></li>
</ul>
    <!--<form class="navbar-form navbar-right">
    <input type="text" class="form-control" placeholder="Search...">
    </form>-->
</div>
</div>
</nav>

<div class="container-fluid">
<div class="row">
<div class="col-sm-3 col-md-2 sidebar">
<ul class="nav nav-sidebar">
    <!-- find users' image if image not found then show dummy image -->
<li><a href="#">
<center><img class="img img-thumbnail" src="../images/admin.png" width="125" height="125" alt="not found" style="border-radius:50px" /></center>
</a> 
</li>
<center> <b><?php echo @$email; ?> </b></center>
<style type="text/css">.text-style-navbar {color: white;border: 2px solid lightgray;font-weight: bolder;text-align: center;}
</style>
    <!-- <li class="active"><a href="index.php" class="text-style-navbar" style="color:black; ">Dashboard <span class="sr-only">(current)</span></a></li> -->
<br>
    <!-- find users' image if image not found then show dummy image -->
    <li class="active"><a href="index.php" class="text-style-navbar">Dashboard <span class="sr-only">(current)</span></a></li><br>

    <li class="active"><a href="index.php?page=notification" class="text-style-navbar"> Mange Notice</a></li><br>

    <li class="active"><a href="index.php?page=manage_teachers" class="text-style-navbar"> Manage Teachers</a></li><br>

    <li class="active"><a href="index.php?page=manage_students" class="text-style-navbar"> Manage Students</a></li><br>

    <li class="active"><a href="index.php?page=update_password" class="text-style-navbar"> Update Password</a></li><br>

</ul>
</div>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <!-- container-->
<?php
    @$page =  $_GET['page'];
if ($page != "")  
{
if ($page == "notification")  
{
include('manage_notice.php');
} 
if ($page == "add new notice")  
{
include('add_new_notice.php');
}
if ($page == "add notice")  
{
include('add_notice.php');
}  
if ($page == "manage notifications")  
{
include('manage_notifications.php');
}
if ($page == "update notice") {
include('update_notice.php');
}
if ($page == "edit notice")  
{
include('edit_notice.php');
}
if ($page == "manage_teachers")  
{
include('manage_teachers.php');
} 
if ($page == "teacher_registration_request")  
{
include('teacher_registration_request.php');
}
if ($page == "teacher_information")  
{
include('teacher_information.php');
} 
if ($page == "manage_students")  
{
include('manage_students.php');
} 
if ($page == "student_registration_request")  
{
include('student_registration_request.php');
}
if ($page == "Student_Information")  
{
include('student_information.php'); 
}
if ($page == "update_password")  
{
include('update_password.php');
}
}  
else  
{
?>

    <!-- container end-->
    <h1 class="page-header">Dashboard</h1>
<div class="row placeholders text-justify"> 

<div class="col-xs-6 col-sm-3 placeholder">
<img src="../images/total teachers.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail"> 
<h2>
<?php
    $result = mysqli_query($conn, "select count(1) FROM teacher");
    $row = mysqli_fetch_array($result);
    $total = $row[0];
    echo $total;
?>
</h2>
    <span class="text-muted">Total Teachers</span>
</div>

<div class="col-xs-6 col-sm-3 placeholder">
<img src="../images/total students.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
<h2>
<?php
    $result = mysqli_query($conn, "select count(1) FROM student");
    $row = mysqli_fetch_array($result);
    $total = $row[0];
    echo $total;
?>
</h2>
    <span class="text-muted">Total Students</span>
</div>

<div class="col-xs-6 col-sm-3 placeholder">
<img src="../images/teachers reg request.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
<h2>
<?php
    $result = mysqli_query($conn, "select count(1) FROM teacher where status='0'");
    $row = mysqli_fetch_array($result);
    $total = $row[0];
    echo $total;
?>
</h2>
<span class="text-muted">Teacher Registration Requests</span>
</div>

<div class="col-xs-6 col-sm-3 placeholder">
<img src="../images/students reg request.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
<h2>
<?php
    $result = mysqli_query($conn, "select count(1) FROM student where status='0'");
    $row = mysqli_fetch_array($result);
    $total = $row[0];
    echo $total;
?>
</h2>
    <span class="text-muted">Student Registration Requests</span>
</div>
</div>
<?php
}
?>
</div>
</div>
</div>

    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/vendor/jquery.min.js"><\/script>')</script>
    <script type="text/javascript" charset="utf8" src="../DataTables/datatables.js"></script>
    <script src="../js/bootstrap.min.js"></script> 

    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../js/vendor/holder.min.js"></script> 

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script> 

</body>
</html>
