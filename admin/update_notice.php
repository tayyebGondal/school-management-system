

<!-- Update Notice Modal -->
<!-- <div class="modal fade" id="updateNoticeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> -->
<?php

$q="select * from notice where id=".$_GET['id']."";
$result = $conn->query($q);
$updateNotice = $result->fetch_assoc();

// if ($result->num_rows > 0) {
//   // output data of each row
//   while($row = $result->fetch_assoc()) {
//    echo $row['id'];
//   }
// }
?>
<style>
	.modal-header {background-color: black;color: white;}
</style> 

<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<div class="col-md-12">
<h3 class="modal-title" style="text-align: center;">Update Notice</h3>
</div>

<!-- <div class="col-md-6">
<button type="button" style="color: white;" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div> -->
</div> 

<div class="modal-body">
<form action="index.php?page=edit notice&&update_id=<?php echo $updateNotice['id'] ?>" method="POST" style="margin-top: 10px;">
				<div class="form-group col-md-12">
					<label for="recipient-name" class="col-form-label">Title:</label>
					<input required type="text" name="subject" value="<?php echo $updateNotice['title'] ?>" class="form-control" id="subject">
				</div>
 
				<div class="form-group col-md-12" 
					<label for="message-text" class="col-form-label">Detail:</label>
					<textarea required class="form-control" name="detail" id="detail"><?php echo $updateNotice['description'] ?></textarea>
				</div> 

				<div class="form-group col-md-12">
					<label for="recipient-name" class="col-form-label">Select User Type:</label>
					<select required name="user-type" onchange="user_types(this)" class="form-control" id="user-type">
						<option value="">Select Type</option>
						<option value="all">All</option>
						<option value="teachers">Teachers</option>
						<option value="students">Students</option>
					</select>
				</div>
				

				<div class="form-group row" style="padding-left: 30px;text-align: left;">
					<button type="submit" class="btn btn-primary">Send</button>
				</div>

				<table class="table table-bordered" id="students-table" style="display: none;width: 100%;">
					<thead class="thead-dark">

						<tr style="background-color: black;color:white">
							<th colspan="4" style="text-align: center;" scope="col">
								<h4>List of all students</h4>
							</th>
						</tr>
						<tr style="background-color: grey;">
							<th scope="col"></th>
							<th scope="col">Sr#</th>
							<th scope="col">User Name</th>
							<th scope="col">Email</th>
						</tr>
					</thead>
					<tbody>
						<?php

						$studentQuery = "SELECT * FROM student";
						$studentResult = $conn->query($studentQuery);
						if ($studentResult->num_rows > 0) {
							$studentSr = 1;
							while ($stundents = $studentResult->fetch_assoc()) { ?>
								<tr>
									<th scope="row">
										<input type="checkbox" name="students[]" value="<?= $stundents['id'] ?>">
									</th>
									<td><?= $studentSr ?></td>
									<td><?= $stundents['name'] ?></td>
									<td><?= $stundents['email'] ?></td>
								</tr>
						<?php
								$studentSr++;
							}
						} ?>
					</tbody>
				</table>
				<table class="table table-bordered" id="teachers-table" style="display: none;width: 100%;">
					<thead class="thead-dark">

						<tr style="background-color: black;color:white">
							<th colspan="4" style="text-align: center;" scope="col">
								<h4>List of all teachers</h4>
							</th>
						</tr>
						<tr style="background-color: grey;">
							<th scope="col"></th>
							<th scope="col">Sr#</th>
							<th scope="col">User Name</th>
							<th scope="col">Email</th>
						</tr>
					</thead>
					<tbody><?php
							$teacherQuery = "SELECT * FROM teacher";
							$teacherResult = $conn->query($teacherQuery);
							if ($teacherResult->num_rows > 0) {
								$teacherSr = 1;
								while ($teachers = $teacherResult->fetch_assoc()) { ?>
								<tr>
									<th scope="row">
										<input name="teachers[]" type="checkbox" value="<?= $teachers['id'] ?>">
									</th>
									<td><?= $teacherSr ?></td>
									<td><?= $teachers['name'] ?></td>
									<td><?= $teachers['email'] ?></td>
								</tr>
						<?php
									$teacherSr++;
								}
							} ?>
					</tbody>
				</table>
			</form>
		</div>
		<!-- <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Send</button>
			</div> -->
	</div>
</div>
<!-- </div> -->


<script>
	function user_types(that) {
		// Destroy student and teacher previous datatable if exist
		$('#students-table').DataTable().destroy();
		$('#teachers-table').DataTable().destroy();
		var user_type = $(that).val();
		if (user_type == 'teachers') {

			//Bootstrap DataTables for paginations and search on teacher table
			$(document).ready(function() {
				$('#teachers-table').DataTable({
					"lengthChange": false,
					"pageLength": 5,
					"order": [
						[1, "asc"]
					],
					columnDefs: [{
						bSortable: false,
						targets: [0, 2, 3]
					}]
				});
			});
			$('#teachers-table_wrapper').show();
			$('#teachers-table').show();
			$('#students-table').hide();
		} else if (user_type == 'students') {

			//Bootstrap DataTables for paginations and search on teacher table
			$(document).ready(function() {
				$('#students-table').DataTable({
					"lengthChange": false,
					"pageLength": 5,
					"order": [
						[1, "asc"]
					],
					columnDefs: [{
						bSortable: false,
						targets: [0, 2, 3]
					}]
				});
			});

			$('#students-table_wrapper').show();
			$('#students-table').show();
			$('#teachers-table').hide();
		} else {
			$('.table').hide();

		}

	}
</script>