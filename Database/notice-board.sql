-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2021 at 05:37 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-board`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `notice_for` text NOT NULL DEFAULT 'all',
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `user`, `notice_for`, `title`, `description`, `date`) VALUES
(6, 'all', 'all', 'welcome', 'wellcome tou you all student and teacher in new session', '2021-10-16 14:25:08'),
(7, 'students', '13,16', 'hello', 'ur exams start from next week', '2021-10-16 14:26:37'),
(8, 'students', '13,14,15', 'hello students', 'ur exams start from next week', '2021-10-16 14:27:45');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobile no` bigint(11) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `usertype` varchar(100) NOT NULL,
  `roll no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `status`, `address`, `email`, `password`, `mobile no`, `gender`, `image`, `dob`, `usertype`, `roll no`) VALUES
(13, 'maryam ', 0, 'pakistan', 'maryam@gmail.com', '123', 3011234567, 'Female', '', '2021-10-01', 'Student', 0),
(14, 'amna', 0, 'pakistan', 'amna@gmail.com', '123', 3012345978, 'Female', 'Screenshot (749).png', '2021-10-01', 'Student', 0),
(15, 'saba', 0, 'pakistan', 'saba@gmail.com', '123', 301234567, 'Female', 'Screenshot (749).png', '2021-10-05', 'Student', 0),
(16, 'ayesha', 0, 'pakistan', 'ayesha@gmail.com', '123', 3012345678, 'Female', 'Screenshot (749).png', '2021-10-08', 'Student', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobile no` bigint(11) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `usertype` varchar(100) NOT NULL,
  `reg id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `name`, `status`, `address`, `email`, `password`, `mobile no`, `gender`, `image`, `dob`, `usertype`, `reg id`) VALUES
(6, 'maryam naz', 0, 'pakistan', 'maryam@gmail.com', '1234', 3011234567, 'Female', 'img.png', '2021-10-07', 'Teacher', 0),
(7, 'amna naz', 0, 'pakistan', 'amna@gmail.com', '1234', 3012345678, 'Female', 'img.png', '2021-10-01', 'Teacher', 0),
(8, 'saba naz', 0, 'pakistan', 'saba@gmail.com', '1234', 3012345689, 'Female', 'img.png', '2021-10-02', 'Teacher', 0),
(9, 'ayesha naz', 0, 'pakistan', 'ayesha@gmail.com', '1234', 3012345679, 'Female', '', '2021-10-04', 'Teacher', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
